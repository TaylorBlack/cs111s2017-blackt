//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Taylor Black 
// Lab #02
// Date: 1/26/17
//
// Purpose: To compute and print out the calculation for how much hair (fluff)         my dog (Roxy) loses after a hair trim. *Disclaimer to Prof. Kapfhammer: My calc.    isn't going to make much sense.*
//*****************************

import java.util.Date; // needed for printing today's date

public class RoxyFluffiness
{
    // main method: program execution begins here
    public static void main(String[] args)
    {

       // Label output with name and date:
       System.out.println("Taylor Black\nLab 2\n" + new Date() + "\n");
       //Variable dictionary:
       int RlbsT = 110;		// Roxy's actual weight b4 hair trim
       int RAge = 10;		// Roxy's current age
       int RlbsEA = 100;	// Roxy's actual weight aft hair trim
       int RoxyFluff;		// Roxy's actual total lost fluff
       int RELF = 5;		// Roxy's estimated lost fluff
       int RlbsF;		// Roxy's total weight loss aft hair trim
       int HMILRAT;		/* How much I still love Roxy aft hair trim */

       //NOTE: ALL CALCULATIONS ARE IN LBS

       // Compute values:
       RlbsF = RlbsT - RlbsEA;
       RoxyFluff = RlbsF + RELF;
       HMILRAT = RAge * RlbsEA;
       
       System.out.println("Roxy's actual weight before hair trim: " + RlbsT);	           System.out.println("Roxy's current age: " + RAge);
       System.out.println("Roxy's actual weight after hair trim: " + RlbsEA);
       System.out.println("Roxy's actual total lost fluff: " + RoxyFluff);
       System.out.println("Roxy's estimated lost fluff: " + RELF);
       System.out.println("Roxy's total weight lost after hair trim: " + RlbsF);
       System.out.println("How much I still love Roxy after hair trim: " + HMILRAT);
    }
}	
