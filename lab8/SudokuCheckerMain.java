 
//***********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Taylor Black
// CMPSC 111 Spring 2017
// Lab 8
// Date: 3/23/17
//
// Purpose: Create a Sudoku board
//***********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed for user input

public class SudokuCheckerMain
{
	public static void main(String[] args) //main method
	{
		//Label output with name and date:
		System.out.println("Kierra Price nd Taylor Black\nLab 8\n" + new Date() + "\n");

                //Intstructions for user
		System.out.println("Welcome to the Sudoku Checker!" + "\n");
		System.out.println("This program checks simple and small 4x4 Sudoku grids for correctness.");
		System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once." + "\n");
		System.out.println("To check your Sudoku, enter your board one row at a time, with each digit");
		System.out.println("separated by a space. Hit ENTER at the end of the row.");


		SudokuChecker checker = new SudokuChecker();
		checker.inputGrid();
		//checker.checkGrid();

		//declaration of variables
		int w1, w2, w3, w4;
		int x1, x2, x3, x4;
		int y1, y2, y3, y4;
		int z1, z2, z3, z4;
	
	}
}

