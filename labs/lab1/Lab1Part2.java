//***************************************
// Taylor Black
// CMPSC 111
// 19 January 2017
// Lab 1
//
// Listing 1.1 from Lewis & Loftus, slightly modified.
// Demonstrates the basic structure of a Java application.
//***************************************

import java.util.Date; //imports the current date

public class Lab1Part2 //public class in gvim using the java program to run in the terminal window
{
    public static void main(String[] args) //allows the program to run
    {
        System.out.println("Taylor Black " + new Date());
        System.out.println("Lab1 Part 2");
        // the two lines above allow my name, current date, and the lab I'm             // working on to be present in the terminal window

        //-----------------------------
        // Prints words of wisdom
        //-----------------------------

        System.out.println("To quote from political activist and author Hellen Keller:");
        System.out.println("Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence.");
       // prints out a Hellen Keller quote and designates the quote as Hellen          // Keller's own words
    }
}
