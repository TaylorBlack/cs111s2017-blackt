//==========================================
// Taylor Black
// CMPSC 111
// 9 February 2017
// Lab 4
// Purpose: To experiment with and practice graphical methods in Java.
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Taylor Black ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

