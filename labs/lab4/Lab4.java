//=================================================
// Taylor Black
// CMPSC 111
// 9 February 2017
// Lab 4
// Purpose: To experiment with and practice graphical methods in Java.
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
     setBackground(Color.pink);

     page.setColor(Color.blue);
     page.fillRect(255, 255, 130, 50);	//fill flower box color
     
     page.setColor(Color.green);
     page.fillRect(310, 155, 25, 100); //fill stem color
     page.drawRect(310, 155, 25, 100); //stem
 
     page.setColor(Color.yellow);
     page.fillOval(275, 125, 100, 30); // fill flower head color
     page.drawOval(275, 125, 100, 30); //flower head

     page.setColor(Color.red);
     page.fillOval(230, 115, 45, 45); // fill flower petal
     page.drawOval(230, 115, 45, 45); //flower petal

     page.setColor(Color.red);
     page.fillOval(260, 85, 45, 45); // fill flower petal
     page.drawOval(260, 85, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(302, 79, 45, 45); // fill flower petal
     page.drawOval(302, 79, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(345, 84, 45, 45); // fill flower petal
     page.drawOval(345, 84, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(375, 113, 45, 45); // fill flower petal
     page.drawOval(375, 113, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(350, 148, 45, 45); // fill flower petal
     page.drawOval(350, 148, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(305, 155, 45, 45); // fill flower petal
     page.drawOval(305, 155, 45, 45); // flower petal

     page.setColor(Color.red);
     page.fillOval(260, 150, 45, 45); // fill flower petal
     page.drawOval(260, 150, 45, 45); // flower petal

     page.drawString("The best and most beautiful things in the world cannot be seen", 110, 40);
     page.drawString("or even touched - they must be felt with the heart.", 150, 65);
     page.drawString("-Helen Keller", 280, 350);

  }
}
