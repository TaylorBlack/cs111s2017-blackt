import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

// NOTE: You do not need to understand all of the working details associated with this program.
// Instead, you should develop a basic understanding of how it works. Please consult with a
// teaching assistant, tutor, or a course instructor if there are parts of the program that you
// do not intuitively understand.

public class MandelbrotBW {
    public static void main(String[] args) throws Exception {
        int width = 1920, height = 1080, max = 1000; //Dimensions (size) of MandelbrotBW.png file
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0x000000, white = 0xFFFFFF; //dictates that .png file is completely black and white

        for (int row = 0; row < height; row++) { //adds another row to the image as long as the row is smaller than the height
            for (int col = 0; col < width; col++) { //adds another column to the image as long as the column is smaller than the width
                double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                int iterations = 0;
                while (x*x+y*y < 4 && iterations < max) {
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iterations++;
                }
                if (iterations < max) image.setRGB(col, row, white);
                else image.setRGB(col, row, black);
		//sets image row as white if the if/else statement finds that the incoming iterations are smaller than the max
            }
        }
        ImageIO.write(image, "png", new File("mandelbrot-bw.png"));
	//outputs a .png file labeled as "mandelbrot-bw.png"
    }
}
