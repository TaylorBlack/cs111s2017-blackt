//******************************
// Honor Code: The work I am submitting is a result of my own thinking and      // efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Lab #03
// Date: 2/01/2017
//
// Purpose: To learn more about variables, expressions, user input, and using   // Scanner methods.
//*****************************
import java.util.Scanner;
import java.util.Date;

public class Lab3
{
    //--------------------
    // Main Method: Calculate restaurant bill input from the user.     
    //--------------------
    public static void main(String[] args)
    {
       //Label output with name and date:
       System.out.println("Taylor Black\nLab 3\n" + new Date() + "\n");

       Scanner scan = new Scanner(System.in);

       // Variable dictionary:
          int Taylor; //Username is Taylor
          double billamount; //Total bill amount
          double percentagetip; //Percent you want to tip
          double tip; // amount user is going to tip
          int hundred = 100; //divide by 100
          double total_bill; // total bill paid
          double people; //splitting the bill among several people

       System.out.print ("Please enter your name: ");
       String name = System.console().readLine();

       System.out.println ("Taylor, welcome to the Tip Calculator!\n\t ");

       System.out.print ("Enter the amount of your bill: $");
       billamount = scan.nextInt();

       System.out.print ("Enter the percentage you want to tip in decimals: ");
       percentagetip = scan.nextDouble();

       tip = percentagetip * billamount;
       billamount = tip + billamount;
       
       System.out.println ("Total tip amount is: $" + tip );
       System.out.println ("Total bill amount is: $" + billamount);
       System.out.println ("How many people are splitting the bill: ");
       people = scan.nextDouble();
       billamount = billamount / people; 
       System.out.println ("Each person should pay: $" + billamount);
       System.out.println ("Have a nice day!");
       

       
    }
}
