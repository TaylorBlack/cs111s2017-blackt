//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Taylor Black
// CMPSC111 Spring 2017
// Purpose: To write one Java program that declares variables of the following         // types: int, double, float, boolean, char, String
//******************************

import java.util.Date;
public class VariableExercise
{
    //-------------------------------
    // Prints variable exercise in class.
    //-------------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
       System.out.println("Taylor Black\nVariableExercise 1\n" + new Date() + "\n");
       // Variables:
       int sides = 3; // declaration with initialization
       System.out.println("A triangle has: " + sides + " sides.");

    }
}
