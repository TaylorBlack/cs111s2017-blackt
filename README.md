# README #

My name is Taylor Black; I am a current freshman in the Class of 2020.

I currently live in Valencia, PA with my parents and my dog, a 10-year-old Newfoundland named Roxy.

My favorite food is tacos, my favorite color is blue, and my hobbies include reading, writing, and using Steam.

Currently on campus, I'm involved in International Club, Pre-Law Club, and a sister of Alpha Delta Pi. I was a member of the Student Advisory Board for the History Department before it met its collective goals and disbanded.

I'm a dreamer that usually sets goals too high for myself to personally achieve.

Currently, I'm hoping I don't fail CS111.