//******************************
// Honor Code: The work I am submitting is a result of my own thinking and         efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Practical 02
// Date: 1/27/2017
//
// Purpose: To create a java program that inserts something 'interesting.' (In     this case I attempt to create a dog head).
//*****************************
import java.util.Date;
public class Practical02
{
  public static void main(String[] args)
  {
      System.out.println("Taylor Black, CMPSC 111\n" + new Date() + "\n");
      System.out.println("  __________");
      System.out.println(" |          \\");
      System.out.println(" |        0  \\____0");
      System.out.println(" |  |          ___|  WOOF!");   
      System.out.println(" |  |_________/");
      System.out.println(" \\__/");
  }
}
