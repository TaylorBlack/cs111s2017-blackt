//******************************
// Honor Code: The work I am submitting is a result of my own thinking and      // efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Lab #06
// Date: 3/2/2017
//
// Purpose: To manipulate strings of Deoxygribonucleic acid (DNA) by            // appropriately using methods from the java.util.String and java.util.Random   // classes.
//*****************************
import java.lang.String;
import java.util.Random;
import java.util.Scanner;
import java.util.Date;

public class Lab6
{
    //---------------------------------
    // Program execution begins here:
    //---------------------------------
    public static void main(String[] args)
    {
    //Variable Dictionary:
    Scanner scan = new Scanner(System.in); // used for input
    Random position = new Random();    //random position generator
    String dnaString, dna2; // user input
    int len;          // length of user input dnaString
    int location;     // one of the positions in dnaString
	
    	System.out.println("Taylor Black\nLab 6 DNA Program\n"
    	+ new Date() + "\n---------------\n");

    	System.out.print("Enter a string (no blanks allowed) containing only C, G, T, and A: ");
    	dnaString = scan.next();

    	dnaString = dnaString.toLowerCase();
    	System.out.println("Convert input to lower case: " + dnaString);
    	//convert dnaString to lower case

    	dnaString = dnaString.toUpperCase();
    	System.out.println("Convert input to upper case: " + dnaString);
    	//convert dnaString to upper case

    	String sequence = dnaString;
    	sequence = sequence.replace('A', 't')
                           .replace('T', 'a')
                           .replace('C', 'g')
                           .replace('G', 'c')
                           .toUpperCase();
            // used to make the complement of user input dnaString
            // through the sequence command

    	System.out.println("Complement of DNA sequence: " + sequence);

    	System.out.println("\n Computing length of given DNA sequence...");
    	len = dnaString.length();
    	System.out.println("     Length of " + dnaString + " = " + len);
    	//computes length of dnaString entered by user into a total

    	System.out.println("\n System is choosing a random position in given DNA sequence length: " + len);
    	location = position.nextInt(len-1);
    	System.out.println("     System chose position: " + (location+1));

		int r = position.nextInt(3);
		char Change = dnaString.charAt(location);
		int New = position.nextInt(3); // used to randomly generate the location in the string "Rand" to pull a char from
		String Rand = "ATCG"; // string to pull the replacement character from

		System.out.println("     System chose " + Rand.charAt(New) + " as replacement for " + Change + "\n");
    	dna2 = dnaString.substring(0,location) + Rand.charAt(New) + dnaString.substring(location+1);
    	String dnaPlus = dnaString.substring(0,location) + Rand.charAt(New) + dnaString.substring(location);
    	String dnaMinus = dnaString.substring(0,location) + dnaString.substring(location+1);
    	// creates the string with the replaced character

		System.out.println("System adds " + Rand.charAt(New) + " at position " + (location+1) + ": " + dnaPlus);
		System.out.println("System removes character at position " + (location+1) + ": " + dnaMinus);
		System.out.println("System changes character at position " + (location+1) + " to " + Rand.charAt(New) + ": " + dna2);


	System.out.println("Thank you for using my program!");
    }
}
