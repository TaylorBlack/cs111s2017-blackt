//*****************************
// CMPSC 111 Spring 2017
// Taylor Black
// Practical 07
// 4/3/17
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    private static int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public static void isLeapYear()
    {
        boolean isLeapYear = ((year % 400 == 0));
    	if (isLeapYear==true)
    	{
    		System.out.println(year + " is a leap year.");
        }
		else
		{
			System.out.println(year + " is not a leap year.");
		}
	}

    // a method that checks if the user's input year is a cicada year
    public static void isCicadaYear()
    {
        boolean isCicadaYear = ((year % 17 == 0));
        if (isCicadaYear == true)
        {
			System.out.println("It's an emergence year for Brood II of the 17-year cicadas.");
		}
		else
		{
			System.out.println("It's not an emergence year for Brood II of the 17-year cicadas.");
		}
	}

    // a method that check if the user's input year is a sunspot year
    public static void isSunspotYear()
    {
        boolean isSunspotYear = ((year % 11 == 0));
		if (isSunspotYear == true)
		{
			System.out.println("It's a peak sunspot year.");
		}
		else
		{
			System.out.println("It's not a peak sunspot year.");
		}
	}
}
