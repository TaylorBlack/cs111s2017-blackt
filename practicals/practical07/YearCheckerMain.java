//*****************************
// CMPSC 111 Spring 2017
// Taylor Black
// Practical 07
// 4/3/17
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main (String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        YearChecker.isLeapYear();
    	YearChecker.isCicadaYear();
    	YearChecker.isSunspotYear();

        System.out.println("Thank you for using this program.");
    }
}
