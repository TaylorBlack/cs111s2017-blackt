// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  // Houses the variables and the locations where exceptions will appear in the 
  // java array
  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;

    try {
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
	ex.printStackTrace;
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
	ex.printStackTrace;
    }
    catch (NullPointerException ex) {
      System.out.println("Null pointer");
	ex.printStackTrace;
    }
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }
    // System tries to catch exceptions and forces an error to appear in the ter    // minal window if the exception is caught. If no exception is caught, the t    // erminal displays short lines of cryptic messages that does not show any e    // xception occurred.

  public static void main(String[] args) {
    int[] X = {0,1,2};
    // throwsExceptions(0, X, "hi");
    // throwsExceptions(10, X, "");
     throwsExceptions(10, X, "bye");
    // throwsExceptions(10, X, null);
    // throwsExceptions(100, X, "computer science");

    // List of exceptions to be thrown into the terminal, which either disrupt t    // he code, or bypasses the java array checking mechanism to display short l    // ines of cryptic messages in the terminal window.
  }

}
