//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Lab 2
// January 26 2017
//
// Purpose: to compute and print the total distance around Softball Bases.
// Purpose: to compute and print the average time around Softball Bases. 
//***********************

import java.util.Date; // needed for printing today's date

public class SamanthaValeraFixed
{
	// main method: program execution begins here
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Samantha Valera\nLab 2\n" + new Date() + "\n");
		System.out.println("Lab 2");  

		// Variables:
		int ftToFirstBase = 60; // distance from home to first base in feet
		int numOfBases = 4; // total number of bases
		int ftToHome;	// distance to home to home in feet
		int secToFirstBase = 3; // average time from home to first base in seconds
		int secToHome; // average time from home to home in seconds
		int mySecToHome = 11; // my fastest time from home to home in seconds
		int difSecToHome; // the difference between my time and the average time from home to home in seconds
		// Compute values:
		ftToHome = ftToFirstBase * numOfBases; // solving for total feet from first base to home 
		secToHome = secToFirstBase + secToFirstBase + secToFirstBase + secToFirstBase; // average time of running from home to home
		difSecToHome = secToHome - mySecToHome; // how much faster I am than the average home to home time
		
		System.out.println("Total Feet from Home to Home Base: " + ftToHome);
		System.out.println("Average Total Seconds from Home to Home Base: " + secToHome); 
		System.out.println("How Much Faster in Seconds I am from Home to Home Base than the Average: " + difSecToHome);
	}
}
