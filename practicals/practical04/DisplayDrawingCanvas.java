//==========================================
// Honor Code: The work I am submitting is a result of my own thinking and      // efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Practical 04
// Date: 2/10/17
// Purpose: To practice creating interactive programs implemented in Java, and  // how to declare variables of different data types and then to change values of// those variables using expressions.
//==========================================

import javax.swing.*;
import java.util.Date;
import java.util.Scanner;

public class DisplayDrawingCanvas {

  // declare variables that can store the user's color values
  public static int redValue;
  public static int greenValue;
  public static int blueValue;

  // define the HEIGHT and WIDTH of the graphic
  public static final int WIDTH = 600;
  public static final int HEIGHT = 400;

  // solicit input from the user on the rectangle's color
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Input the Red Value: ");
    redValue = scan.nextInt();

    System.out.print("Input the Green Value: ");
    greenValue = scan.nextInt();

    System.out.print("Input the Blue Value: ");
    blueValue = scan.nextInt();

    JFrame window = new JFrame("Taylor Black " + new Date());

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new PaintDrawingCanvas());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(WIDTH, HEIGHT);
  }
}

