//******************************
// Honor Code: The work I am submitting is a result of my own thinking and      // efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Practical 06
// Date: 3/31/2017
//
// Purpose: To have the user guess a series of numbers until they guess the
// correct number.
//*****************************
import java.util.Random;
import java.util.Scanner;
import java.util.Date;

public class MyGuessingGame
{
    //--------------------
    // main method: program execution begins here
    //--------------------
    public static void main(String[] args)
    {
       //Label output with name and date:
       final int MAX = 100;
       int answer, guess;
       int numberOfTries = 0;
       int success = 0;
       int choice = 0;
       boolean playAgain = true;

             System.out.println("Taylor Black\nPractical 06\n"
             + new Date() + "\n-----------------\n");

       Scanner scan = new Scanner(System.in);
       Random generator = new Random();

       answer = generator.nextInt(MAX) + 1;

		do
		{
			do
			{
       			System.out.print("I'm thinking of a number between 1 and " + MAX + ". Guess what it is: ");
				guess = scan.nextInt();
       			numberOfTries++;

        	    if (guess == answer)
        	    {
        	    	success++;
        	        System.out.println("You got it! Good guessing! Your number of tries was: " + numberOfTries + " and the number was: " + guess);
        	    }

        	    else if (guess < answer)
        	    {
        	        System.out.println("Your guess is too low! Try again.\n");
        	    }

        	    else if (guess > answer)
        	    {
        	        System.out.println("Your guess is too high! Try again.\n");
        		}
        	}while (success == 0);

        	System.out.println();
        	System.out.print("Would you like to run the program again? (1:Yes/2:No)");
        	String repeat = scan.next();
        	switch (repeat.toLowerCase())
        	{
        		case "yes":
        	    playAgain = true;
        	    break;
        	    default:
        	    playAgain = false;
        	    break;
        	}
		}while (playAgain == true);
    }
}
