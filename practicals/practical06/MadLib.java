//******************************
// Honor Code: The work I am submitting is a result of my own thinking and      // efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Practical #06
// Date: 2/24/2017
//
// Purpose: To develop a MadLib program and practice String classes.
//*****************************
import java.util.Scanner;
import java.util.Date;

public class MadLib
{
    //--------------------
    // main method: program execution begins here
    //--------------------
    public static void main(String[] args)
    {
       //Label output with name and date:
         System.out.println("Taylor Black\nMadLib\n" + new Date() + "\n");



       // Variable dictionary:
       int num;          //user inputs a whole number
       String disaster;  //user inputs a natural disaster
       String adjective; //user inputs an adjective
       int year;      //user inputs a year
       int total;        //total for calculation
      
       Scanner scan = new Scanner(System.in);

       System.out.print("Enter a whole number: ");
       num = scan.nextInt();

       System.out.print("Enter a type of natural disaster: ");
       disaster = scan.next();

       System.out.print("Enter an adjective: ");
       adjective = scan.next();

       System.out.print("Enter a year: ");
       year = scan.nextInt();

       System.out.println(" ");
       System.out.println("The Only Day School Was Ever Cancelled ");

       //Compute values:
       total = year - num;

       System.out.println("School was cancelled in the year " + year);
       System.out.println("I believe at the time I was only " + total);
       System.out.println("Why was school cancelled, you ask? ");
       System.out.println("Well, the school was hit by a " + disaster);
       System.out.println("It was totally " + adjective);
       System.out.println("Now I've babbled on too long. You're going to be late to school!"); 
       

       

    }
}
